Ansible Role: Supervisor
===================
Install and configure supervisor on Debian servers

It Install supervisopr and configure the supervisor queue. It will also configure a `notify_fatal` command if requested.

----------
Requirment
--------------

If notify fatal is requested you need to deploy a script on the machine in order to allow supervisor to execute the command when needed.

Role variables
-------------
Available variables are listed below, along with default values (see defaults/main.yml):

	supervisor_log_file: /var/log/supervisor/supervisord.log
	supervisor_child_log_dir: /var/log/supervisor
	supervisor_max_log_file: 50MB
	supervisor_log_backup: 10
These are some default value of supervisor configuration. For additional details see the supervisord [documentation](http://supervisord.org/configuration.html)

	supervisor_notify_fatal_command: <n/a>
This variable is disabled by default, if provided, supervisor will execute this command on failure of its queue.

	supervisor_queue: []
A list of queues supervisor would handle. Each element have these fields whit the respective default:

	    - name: <n/a>
	      command: <n/a>
	      autostart: "true"
	      autorestart: "true"
	      exitcodes: 0
	      redirect_stderr: "true"
	      error_log: <n/a>
	      out_log: <n/a>
	      user: <n/a>
	      numprocs: {{ansible_processor_count}}
	      startsecs: 0

   
Dependencies
-------------------
None.

Example Playbook
--------------------------

    - name: deploy supevisor
      hosts: webservers
      roles:
        - role: supervisor
          supervisor_notify_fatal_command: "php /var/www/exemple.com/scripts/supervisormon/notify.php"
          supervisor_queue:
            - name: exemple_com
              command: "php /var/www/exemple.com/laravel/artisan queue:work database --sleep=3 --tries=3"
              error_log: /var/www/exemple.com/laravel/storage/logs/exemple_com.err.log
              out_log: /var/www/exemple.com/laravel/storage/logs/exemple_com.out.log"
              user: www-data
              numprocs: 2
 